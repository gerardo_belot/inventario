@extends('layouts.admin')


@section('labels')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item "><a href="/financial-accounts/">{{ __('Financial Accounts') }}</a></li>
        <li class="breadcrumb-item active">{{ __($account->description) }}</li>
    </ol>
</nav>
@stop

@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Financial Accounts</h6>
    </div>
    <div class="card-body">
        <p>
            {{ $account->description }}
            <hr>
            {{ $account->childrens }}
        </p>
    </div>
</div>
@stop
