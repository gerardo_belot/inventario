@extends('layouts.admin')


@section('labels')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">{{ __('Accounts Catalog') }}</li>
    </ol>
</nav>
@stop

@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3 clearfix">
        <p class="m-0 font-weight-bold text-primary d-sm-flex align-items-center justify-content-between">
            {{__('Accounts Catalog')}}
            <a href="/financial-accounts/create" class="btn btn-primary pull-right text-right"><i class="fas fa-plus"></i></a>
        </p>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-hover">
            <thead>
            <th>Account Code</th>
            <th>Descripcion</th>
            <th>Group</th>
            <th>Action</th>
            </thead>
            <tbody>
                @foreach($accounts as $account)
                <tr>
                    <td>{{ $account->code }}</td>
                    <td>{{ $account->description }}</td>
                    <td>{{ $account->base->description }}</td>
                    <td>Edit</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
