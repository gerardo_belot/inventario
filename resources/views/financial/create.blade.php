@extends('layouts.admin')


@section('labels')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item "><a href="/financial-accounts/">{{ __('Accounts Catalog') }}</a></li>
            <li class="breadcrumb-item active">{{ __('Create Account') }}</li>
        </ol>
    </nav>
@stop

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create Accounts</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['url' => '/financial-accounts']) }}

            <div class="row">

                <div class="col-md-3">
                    <label>Description</label>
                    {{ Form::text('description', null, ['class' => 'form-control']) }}
                </div>

                <div class="col-md-3">
                    <label>Account Number</label>
                    {{ Form::text('account', null, ['class' => 'form-control']) }}
                </div>

                <div class="col-md-4 form-group">
                    <label>Group Account</label>
                    {{ Form::select('type_accounts_id', $type, null, ['class' => 'form-control', 'id' => 'type_accounts']) }}
                </div>

                <br>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Parent Account</label>
                    {{ Form::select('parent_id', $accounts, null, ['class' => 'form-control select2',
                    'id' => 'perent_select', 'disabled' => 'disabled']) }}
                </div>

                <div class="col-md-3 form-group">
                    <label>Base Account</label>
                    {{ Form::select('base_accounts_id', $base, null, ['class' => 'form-control', 'id' => 'base_accounts']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <br>
                    <button type="submit" class="btn btn-primary">Crear</button>
                    <button type="submit" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-inline">
                            <input type="text" class="form-control" id="recipient-name" placeholder="Search Account">
                        </div>
                        <div class="form-group">
                            <br>
                            <table id="account_results" class="table table-bordered">
                                <thead>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>Select</th>
                                </thead>
                                <tbody id="account_body">
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
@stop
