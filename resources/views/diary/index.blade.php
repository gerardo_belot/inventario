@extends('layouts.admin')


@section('labels')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item active">{{ __('Diary Book') }}</li>
        </ol>
    </nav>
@stop

@section('content')
    <journal></journal>
@stop