@extends('layouts.admin')


@section('labels')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item active">{{ __('Providers') }}</li>
        </ol>
    </nav>
@stop

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Providers List</h6>
        </div>
        <div class="card-body">
            <div  class="table-responsive">
                <table id="providers_datatable" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>{{ __('Name')  }}</th>
                        <th>{{ __('P.O.S.')  }}</th>
                        <th>{{ __('Website')  }}</th>
                        <th>{{ __('Action')  }}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop