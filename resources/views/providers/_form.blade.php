<div class="row">

    <div class="col-md-12 form-group">
        <label for="name">{{ __('Name') }}</label>
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-12 form-group">
        <label for="name">{{ __('Website') }}</label>
        {{ Form::text('website', null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-6 form-group">
        <label for="email">{{ __('e-mail') }}</label>
        {{ Form::text('email', null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-6 form-group">
        <label for="pos">{{ __('P.O.S. (phone)') }}</label>
        {{ Form::text('phone', null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-12 form-group">
        <label for="pos">{{ __('Address') }}</label>
        {{ Form::textarea('address', null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-12">
        <button class="btn btn-primary" type="submit">{{ $update }}</button>
        <a href="/providers" class="btn btn-danger">Cancel</a>
    </div>
</div>