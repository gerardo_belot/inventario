@extends('layouts.admin')


@section('labels')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/providers">{{ __('Providers') }}</a></li>
            <li class="breadcrumb-item active">{{ $provider->name }}</li>
        </ol>
    </nav>
@stop

@section('content')
    <div class="row">
        <div class="card shadow mb-4 mr-2 col-6">
            <div class="card-header py-3">
                <h6 class="mb-3 font-weight-bold text-primary">{{ __('General Info') }}</h6>
                {!! Form::model($provider, ['action' => ['ProviderController@update', $provider->id], 'method' => 'put']) !!}
                    @include('providers._form', ['update' => 'update'])
                {!! Form::close() !!}
            </div>
        </div>

        <div class="card shadow mb-4 mr-1 col-5">
            <div class="card-header py-3">
                <h6 class="mb-3 font-weight-bold text-primary">{{ __('Contacts Info') }}</h6>

                <div class="list-group">
                    @foreach($provider->contacts as $contact)
                    <a href="#" class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between text-info">
                            <h5 class="mb-1">{{ $contact->name }} </h5>
                            <small>{{ $contact->position }}</small>
                        </div>
                        <p class="mb-0">E-mail: {{ $contact->email }}</p>
                        <p class="mb-0">Phone: {{ $contact->phone }} - Extention: {{ $contact->ext }}</p>
                        <p>Mobile: {{ $contact->mobile }}</p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@stop