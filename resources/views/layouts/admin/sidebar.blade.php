<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-th-large"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel') }}<sup>1</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard  titlulo-->
    <li class="nav-item active">
        <a class="nav-link" href="/home">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
@if(auth()->user()->can('ver_usuarios') || auth()->user()->can('ver_roles'))

    <!-- Heading -->
        <div class="sidebar-heading">
            {{ __('Configurations') }}
        </div>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
               aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>{{ __('Users and Security') }}</span>
            </a>
        @if(Request::is('users') || Request::is('users/*') || Request::is('roles') || Request::is('roles/*'))
            <!-- {{ $usuarios = 'tr' }} -->
            @endif

            <div id="collapseTwo" class="collapse {{ isset($usuarios) ? 'show' : '' }}" aria-labelledby="headingTwo"
                 data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">{{ __('User & Permissions') }}:</h6>
                    @can('ver_usuarios')
                        <a class="collapse-item {{ Request::is('users') ? 'active' : '' }}"
                           href="/users">{{ __('Users List') }}</a>
                    @endcan
                    @can('ver_roles')
                        <a class="collapse-item {{ Request::is('roles') ? 'active' : '' }}"
                           href="/roles">{{ __('Roles & Permissions') }}</a>
                    @endcan
                </div>
            </div>
        </li>
@endif

<!-- Heading -->
    <div class="sidebar-heading">
        {{ __('Accounting') }}
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsefour"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>{{ __('Accounts') }}</span>
        </a>
    @if(Request::is('financial-accounts') || Request::is('financial-accounts/*') || Request::is('diary-book'))
        <!-- {{ $financial = 'tr' }} -->
        @endif

        <div id="collapsefour" class="collapse {{ isset($financial) ? 'show' : '' }}" aria-labelledby="headingTwo"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">{{ __('Accounts Settings') }}:</h6>
                {{-- @can('ver_usuarios')--}}
                <a class="collapse-item {{ Request::is('financial-accounts') ? 'active' : '' }}"
                   href="/financial-accounts">{{ __('Account Catalog') }}</a>
                {{--@endcan--}}
                {{-- @can('ver_roles')--}}
                <a class="collapse-item {{ Request::is('diary-book') ? 'active' : '' }}"
                   href="/diary-book">{{ __('Diary Book') }}</a>
                {{--@endcan--}}
            </div>
        </div>
    </li>


<!-- Heading -->
    <div class="sidebar-heading">
        {{ __('Inventory') }}
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTree"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>{{ __('Buy and Sell Form') }}</span>
        </a>
        @if(Request::is('categories') || Request::is('providers') || Request::is('providers/*'))
            <!-- {{ $category = 'tr' }} -->
        @endif

        <div id="collapseTree" class="collapse {{ isset($category) ? 'show' : '' }}" aria-labelledby="headingTwo"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">{{ __('Invetory Settings') }}:</h6>
               {{-- @can('ver_usuarios')--}}
                    <a class="collapse-item {{ Request::is('categories') ? 'active' : '' }}"
                       href="/categories">{{ __('Categories List') }}</a>
                {{--@endcan--}}
               {{-- @can('ver_roles')--}}
                   <a class="collapse-item {{ Request::is('providers') ? 'active' : '' }}"
                       href="/providers">{{ __('Providers') }}</a>
                {{--@endcan--}}
            </div>
        </div>
    </li>

<!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->

