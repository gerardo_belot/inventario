$(document).ready( function () {

    let name = $('#edit_category_modal #name');
    let desc = $('#edit_category_modal #description');
    let id;

    let table = $('#categories_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/categories",
        "order": [[0, "desc"]],
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'description', name: 'description' },
            { data: 'actions', name: 'action', "searchable": false }
        ],
        "fnDrawCallback": function(oSettings){
            clickable();
        }
    });

    function clickable(){
        $('.category_modal').click(function (e) {
            e.preventDefault();
            id = $(this).attr('href');
            axios.get('/categories/' + id).then(resp => {
                console.log(resp.data);
                name.val(resp.data.name);
                desc.val(resp.data.description);
                $('#edit_category_modal').modal({keyboard: false, backdrop: 'static'});
            });

        });
    }

    $('#push_category').on('click', function(e){
        data = {
            name: name.val(),
            description: desc.val()
        }

        axios.put('categories/' + id, data)
            .then(resp => {
                table.ajax.reload();
                $('#edit_category_modal').modal('hide')
            }).catch(e => {
                $('#edit_category_modal').modal('hide')
                table.ajax.reload();
                alert(e)
        });
    });
    
    $('#category_add').on('click', function (e) {
        e.preventDefault();
        $('#add_category_modal').modal({keyboard: false, backdrop: 'static'});
    })

    $('#add_category_modal #push_category').on('click', function(e){
        data = {
            name: $('#add_category_modal #name').val(),
            description: $('#add_category_modal #description').val(),
        };

        if (data.name !== ""){
            axios.post('/categories', data)
                .then(resp => {
                    table.ajax.reload();
                    $('#add_category_modal').modal('hide')
                }).catch(e => {
                    $('#add_category_modal').modal('hide')
                    table.ajax.reload();
                    alert(e)
            });
        }
    });

});