$(document).ready( function () {

    let table = $('#providers_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/providers",
        "order": [[0, "desc"]],
        columns: [
            { data: 'name', name: 'name' },
            { data: 'phone', name: 'phone' },
            { data: 'website', name: 'website' },
            { data: 'actions', name: 'actions', "searchable": false },
        ],
    });

});