import axios from 'axios'
$(document).ready( function () {

    $('#type_accounts').change(function () {
        if($(this).val() === '1'){
            $('#perent_select').val("");
            $('#perent_select').prop('disabled', true)
        } else {
            $('#perent_select').prop('disabled', false);
        }

    })

    $('#perent_select').select2({
        placeholder: "Choose Parent Account...",
        minimumInputLength: 2,
        ajax: {
            url: '/financial-accounts',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term),
                    t: $('#type_accounts').val() - 1
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            //cache: true
        }
    })
});