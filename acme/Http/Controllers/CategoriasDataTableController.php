<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09-11-20
 * Time: 10:46 AM
 */

namespace Acme\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CategoriasDataTableController
{
    /**
     * @return mixed
     */
    public function CategoryDataTable()
    {
        $build = DB::table('categories');
        return dataTables::of($build)
            ->addColumn('actions', function ($name) {
                return '
                <a class="category_modal" href="' . $name->id . '"><i class="fas fa-edit"></i> Edit</a>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}