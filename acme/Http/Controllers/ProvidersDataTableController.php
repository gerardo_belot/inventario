<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 09-16-20
 * Time: 10:08 AM
 */

namespace Acme\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables as dataTables;

class ProvidersDataTableController
{

    /**
     * @return mixed
     */
    public function providerDataTable()
    {
        $build = DB::table('providers');
        return dataTables::of($build)
            ->addColumn('actions', function ($name) {
                return '
                <a class="" href="/providers/' . $name->id . '/edit"><i class="fas fa-edit"></i> Edit</a>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}