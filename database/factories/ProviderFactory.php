<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Provider;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'email' => $faker->companyEmail,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url
    ];
});
