<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ContactProvider;
use Faker\Generator as Faker;

$factory->define(ContactProvider::class, function (Faker $faker) {
    return [
        'provider_id'   => 1,
        'name'          => $faker->name,
        'position'      => $faker->jobTitle,
        'ext'           => $faker->randomDigit,
        'phone'         => $faker->phoneNumber,
        'mobile'        => $faker->phoneNumber,
        'email'         => $faker->email
    ];
});
