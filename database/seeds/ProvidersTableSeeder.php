<?php

use App\Provider;
use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('providers')->delete();
        
        factory(Provider::class, 20)->create();
        for($i = 1; $i < 21; $i++){
            factory(\App\ContactProvider::class)->create(['provider_id' => $i]);         }
        }
}