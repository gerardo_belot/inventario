<?php

use Illuminate\Database\Seeder;

class TypeAccountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('type_accounts')->delete();
        
        \DB::table('type_accounts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'description' => 'Financieras',
                'created_at' => '2020-10-11 16:30:22',
                'updated_at' => '2020-10-11 16:30:23',
            ),
            1 => 
            array (
                'id' => 2,
                'description' => 'Control',
                'created_at' => '2020-10-11 16:30:31',
                'updated_at' => '2020-10-11 16:30:32',
            ),
            2 => 
            array (
                'id' => 3,
                'description' => 'Detalle',
                'created_at' => '2020-10-11 16:30:40',
                'updated_at' => '2020-10-11 16:30:41',
            ),
            3 => 
            array (
                'id' => 4,
                'description' => 'Movimiento',
                'created_at' => '2020-10-11 16:30:54',
                'updated_at' => '2020-10-11 16:30:55',
            ),
        ));
        
        
    }
}