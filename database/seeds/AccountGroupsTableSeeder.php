<?php

use Illuminate\Database\Seeder;

class AccountGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('account_groups')->delete();
        
        \DB::table('account_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cuentas Base',
                'created_at' => '2020-10-10 22:47:48',
                'updated_at' => '2020-10-10 22:47:49',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Cuentas Financieras',
                'created_at' => '2020-10-10 22:48:00',
                'updated_at' => '2020-10-10 22:48:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cuentas Control',
                'created_at' => '2020-10-10 22:48:09',
                'updated_at' => '2020-10-10 22:48:09',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Cuentas Detalle',
                'created_at' => '2020-10-10 22:48:20',
                'updated_at' => '2020-10-10 22:48:20',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Cuentas de Movimiento',
                'created_at' => '2020-10-10 22:48:31',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}