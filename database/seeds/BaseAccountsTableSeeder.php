<?php

use Illuminate\Database\Seeder;

class BaseAccountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('base_accounts')->delete();
        
        \DB::table('base_accounts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'description' => 'Activos',
                'created_at' => '2020-10-07 17:28:47',
                'updated_at' => '2020-10-07 17:28:54',
            ),
            1 => 
            array (
                'id' => 2,
                'description' => 'Pasivos',
                'created_at' => '2020-10-07 17:29:03',
                'updated_at' => '2020-10-07 17:29:06',
            ),
            2 => 
            array (
                'id' => 3,
                'description' => 'Patrimonio',
                'created_at' => '2020-10-07 17:29:23',
                'updated_at' => '2020-10-07 17:29:26',
            ),
            3 => 
            array (
                'id' => 4,
                'description' => 'Ingresos',
                'created_at' => '2020-10-07 17:29:41',
                'updated_at' => '2020-10-07 17:29:43',
            ),
            4 => 
            array (
                'id' => 5,
                'description' => 'Costos',
                'created_at' => '2020-10-07 17:29:54',
                'updated_at' => '2020-10-07 17:29:55',
            ),
            5 => 
            array (
                'id' => 6,
                'description' => 'Gastos',
                'created_at' => '2020-10-07 17:30:07',
                'updated_at' => '2020-10-07 17:30:09',
            ),
        ));
        
        
    }
}