<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Vegetales',
                'description' => 'Vegetales',
                'created_at' => '2020-09-12 16:51:38',
                'updated_at' => '2020-09-12 16:51:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'autem',
                'description' => 'Dolorem quidem quos et eligendi laudantium fuga sit.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'delectus',
                'description' => 'Id deleniti excepturi ut provident fugiat.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'ab',
                'description' => 'Ipsum et dolorum expedita unde.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'ratione',
                'description' => 'Aut sint dignissimos ab fugiat harum tenetur adipisci.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'consequatur',
                'description' => 'Beatae at expedita repellendus qui adipisci autem.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'iusto',
                'description' => 'Illo iste facilis nostrum maiores voluptates.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'culpa',
                'description' => 'Animi alias veniam deleniti quo quo voluptatem voluptas.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'ea',
                'description' => 'Et occaecati nobis tempora neque.',
                'created_at' => '2020-09-12 20:52:37',
                'updated_at' => '2020-09-12 20:52:37',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'iure',
                'description' => 'Voluptatem saepe nemo et qui non ut et.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'in',
                'description' => 'Animi quo aut iusto occaecati eveniet.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'non',
                'description' => 'Sed accusamus dicta aspernatur.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'autem',
                'description' => 'Dicta est commodi explicabo.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'odio',
                'description' => 'Architecto cumque voluptatum aut fugiat aperiam autem.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'perspiciatis',
                'description' => 'Consequatur aspernatur consequatur a sed aut dolorem id.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'quibusdam',
                'description' => 'Fugiat nemo eius et aliquam quo ad.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'et',
                'description' => 'Itaque in dolores voluptatem unde quia accusantium.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'atque',
                'description' => 'Ex quidem excepturi dolores quisquam omnis voluptatem.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'rerum',
                'description' => 'Maiores sint delectus animi et et mollitia.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'aliquid',
                'description' => 'Rerum perspiciatis error in optio ipsa.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'rerum',
                'description' => 'Deleniti voluptatibus eos assumenda et provident mollitia.',
                'created_at' => '2020-09-12 20:52:38',
                'updated_at' => '2020-09-12 20:52:38',
            ),
        ));
        
        
    }
}