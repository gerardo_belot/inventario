<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('base_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('type_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('financial_accounts', function (Blueprint $table) {
            $table->id();
            $table->integer('base_accounts_id');
            $table->integer('type_accounts_id');
            $table->integer('account')->index();
            $table->BigInteger('code')->unique()->index();
            $table->integer('parent_id')->nullable();
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return voidº
     */
    public function down()
    {
        Schema::dropIfExists('financial_accounts');
        Schema::dropIfExists('type_accounts');
        Schema::dropIfExists('base_accounts');
    }
}
