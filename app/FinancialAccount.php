<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class FinancialAccount extends Model  implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    /**
     * @var array
     */
    protected $fillable = ['account', 'code', 'parent_id', 'base_accounts_id', 'type_accounts_id', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(FinancialAccount::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrens()
    {
        return $this->hasMany(FinancialAccount::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function base()
    {
        return $this->belongsTo(BaseAccount::class, 'base_accounts_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(TypeAccount::class, 'type_accounts_id', 'id');
    }
}
