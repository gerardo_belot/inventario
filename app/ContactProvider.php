<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContactProvider extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['name', 'title', 'ext', 'phone', 'mobile', 'email', 'position', 'provider_id'];

    //TODO: CRUD de ContactsProviders
}
