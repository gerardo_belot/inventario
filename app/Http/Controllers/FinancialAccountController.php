<?php

namespace App\Http\Controllers;

use App\BaseAccount;
use App\FinancialAccount;
use App\TypeAccount;
use Illuminate\Http\Request;
use Illuminate\View\View;

class FinancialAccountController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $term = $request->get('q');
            $type = $request->get('t');

            if (empty($term)) {
                return response()->json([]);
            }

            $tags = FinancialAccount::where('description', 'like', '%' . $term . '%')
                ->where('type_accounts_id', $type)
                ->limit(10)->get();

            $formatted_tags = [];

            foreach ($tags as $tag) {
                $desc = $tag->code . " " . $tag->description;
                $formatted_tags[] = ['id' => $tag->id, 'text' => $desc];
            }

            //return \Response::json($formatted_tags);
            return response()->json($formatted_tags, 200);
        }

        $title = "Account Catalog";
        $accounts = FinancialAccount::orderBy('code', 'DESC')->paginate(30);
        $base = BaseAccount::all()->pluck('description', 'id');
        $type = TypeAccount::all()->pluck('name', 'id');
        return View('financial.index', compact('accounts', 'base', 'type', 'title'));
    }

    public function create()
    {
        $accounts = [];
        $base = BaseAccount::all()->pluck('description', 'id');
        $type = TypeAccount::all()->pluck('description', 'id');
        return view('financial.create', compact('base', 'type', 'accounts'));
    }

    public function store(Request $request)
    {

        if ($request->get('type_accounts_id') === 1) {
            $length = strlen((string)$request['account']);
            if ($length == 1) {
                $request['code'] = $request['base_accounts_id'] . 0 . $request['account'];
            } else {
                $request['code'] = $request['base_accounts_id'] . $request['account'];
            }
        }

        if ($request->get('type_accounts_id') == 2 || $request->get('type_accounts_id') == 3) {
            $length = strlen((string)$request['account']);
            $parent_code = FinancialAccount::find($request->get('parent_id'));
            if ($length == 1) {
                $request['code'] = $parent_code->code . 0 . $request['account'];
            } else {
                $request['code'] = $parent_code->code . $request['account'];
            }
        }

        if ($request->get('type_accounts_id') == 4) {
            $length = strlen((string)$request['account']);
            $parent_code = FinancialAccount::find($request->get('parent_id'));
            if ($length == 1) {
                $request['code'] = $parent_code->code . 000 . $request['account'];
            } elseif ($length == 2) {
                $request['code'] = $parent_code->code . 00 . $request['account'];
            } elseif ($length == 3) {
                $request['code'] = $parent_code->code . 0 . $request['account'];
            } elseif ($length == 4) {
                $request['code'] = $parent_code->code .  $request['account'];
            }
        }


        $account = FinancialAccount::create($request->all());
        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show($id)
    {
        $account = FinancialAccount::findOrFail($id);
        return View('financial.show', compact('account'));
    }
}
