<?php

namespace App\Http\Controllers;

use Acme\Http\Controllers\CategoriasDataTableController;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        // Definimos permisos de ingreso
        $this->middleware(['role:Administrator|Supervisor']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Desplegamos lista de Datatable
        if($request->ajax()){
            $table = new CategoriasDataTableController();
            return $table->CategoryDataTable();
        }

        //Definimos titulo de la sección;
        $title = 'Categories';

        return view('categories/index', compact('title'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $cat = Category::create($request->validate([
            'name' => 'required',
            'description' => 'string'
        ]));

        return 'done';
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $cat = Category::findOrFail($id);
        return response()->json($cat, 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $cat = Category::findOrFail($id);
        $cat->update($request->all());
        return response()->json($cat, 200);
    }

}
