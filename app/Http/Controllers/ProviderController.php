<?php

namespace App\Http\Controllers;

use Acme\Http\Controllers\ProvidersDataTableController;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()) {
            $table = new ProvidersDataTableController();
            return $table->providerDataTable();
        }
        $title = "Providers";
        return view('providers/index', compact('title'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $provider = Provider::findOrFail($id);
        $title = $provider->name;
        return view('providers/edit', compact('title', 'provider'));
    }


    public function update(Request $request, $id)
    {
        $provider = Provider::findOrFail($id);
        $provider->update($request->validate([
            'name'      => 'required',
            'phone'     => 'nullable',
            'address'   => 'nullable',
            'email'     => 'nullable|email',
            'website'   => 'nullable'
        ]));

        return response()->redirectTo('/providers');
    }
}
