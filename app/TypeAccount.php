<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class TypeAccount extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    /**
     * @var string
     */
    protected $table = 'type_accounts';


    public function accounts()
    {
        return $this->hasMany(FinancialAccount::class, 'type_accounts_id', 'id');
    }


}
