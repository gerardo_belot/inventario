<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BaseAccount extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'base_accounts';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(FinancialAccount::class, 'base_accounts_id', 'id');
    }

}
